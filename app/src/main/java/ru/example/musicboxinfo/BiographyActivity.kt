package ru.example.musicboxinfo

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.google.android.material.button.MaterialButton
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.URL
import java.security.SecureRandom
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext


class BiographyActivity : AppCompatActivity() {
    private var searchString: String? = ""
    private var searchStringFlag: Boolean = false

    companion object {
        const val EXTRA_SEARCH_2 = "ru.example.musicboxinfo.EXTRA_SEARCH_2"
    }

    interface MyInterface {
        fun myMethod( result: List<String> )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.biography_search)

        searchString = intent.getStringExtra( EXTRA_SEARCH_2 )

        searchStringFlag = false

        var eText = findViewById<EditText>(R.id.editText_search2)

        eText.setText( searchString )

        eText.doOnTextChanged { text, start, count, after ->
            //Log.d("STATE", "doOnTextChanged: $text, $start, $count, $after")
            if( text?.isNotEmpty() == true && searchStringFlag ) {
                findViewById<EditText>(R.id.editText_search2).text.clear()
                searchStringFlag = false
            }
        }

        doSearch( findViewById<MaterialButton>(R.id.btn_search2) )
   }

    fun doSearch( v: View ) {
        searchString = findViewById<EditText>(R.id.editText_search2).text.toString()

        //Log.d("STATE", "doSearch: searchString='$searchString'")

        if( searchString == null || searchString.equals("") ) return

        val re = Regex("[^A-Za-zА-Яа-я0-9- ]")
        searchString = re.replace( searchString!!, "" )
        searchString = searchString?.trim()
        //Log.d("STATE", "searchString: '$searchString'")

        AsyncRequest(applicationContext, object : MyInterface {
            override fun myMethod(result: List<String>) {
                try {
                    findViewById<TextView>(R.id.textView_name2).setText( result[0] )
                    findViewById<TextView>(R.id.textView_text2).setText( result[1] )
                    findViewById<TextView>(R.id.textView_text2).setMovementMethod( ScrollingMovementMethod() )
                }
                catch ( ex: Exception ) {
                    Log.e("ERROR", ex.message.toString())
                    //ex.printStackTrace()
                }
            }
        }).execute( "https://ws.audioscrobbler.com/2.0/?method=artist.getinfo&api_key=a51017fe06c1f177a83deaf4ca409325&artist=$searchString&lang=ru&format=json", "1" )

        AsyncRequest(applicationContext, object : MyInterface {
            override fun myMethod(result: List<String>) {
                try {
                    DownloadImageFromInternet(findViewById(R.id.imageView_photo2)).execute( result[0] )
                }
                catch ( ex: Exception ) {
                    Log.e("ERROR", ex.message.toString())
                    //ex.printStackTrace()
                }
            }
        }).execute("https://ws.audioscrobbler.com/2.0/?method=artist.getTopAlbums&api_key=a51017fe06c1f177a83deaf4ca409325&artist=$searchString&format=json", "2" )

        searchStringFlag = true
    }

    fun goBack( v: View ) {
        val intent = Intent ( this, MainActivity::class.java )

        intent.putExtra( MainActivity.EXTRA_SEARCH_1, searchString )

        startActivity( intent )
    }

    @SuppressLint("StaticFieldLeak")
    @Suppress("DEPRECATION")
    private inner class DownloadImageFromInternet(var imageView: ImageView) : AsyncTask<String, Void, Bitmap?>() {
        /* init {
            Toast.makeText(applicationContext, "Please wait, it may take a few minute...", Toast.LENGTH_SHORT).show()
        }*/
        override fun doInBackground(vararg urls: String): Bitmap? {
            val imageURL = urls[0]
            var image: Bitmap? = null
            try {
                val my_in1 = URL(imageURL).openStream()
                image = BitmapFactory.decodeStream( my_in1 )
            }
            catch (e: Exception) {
                Log.e("ERROR", e.message.toString())
                //e.printStackTrace()
            }
            return image
        }
        override fun onPostExecute(result: Bitmap?) {
            imageView.setImageBitmap(result)
        }
    }

    @SuppressLint("StaticFieldLeak")
    @Suppress("DEPRECATION")
    private inner class AsyncRequest(
        applicationContext: Context,
        private var mListener: MyInterface?
    ) : AsyncTask<String?, Int?, List<String>>() {

        init {
            Toast.makeText(applicationContext, "Пожалуйста подождите, это может занять некоторое время...", Toast.LENGTH_SHORT).show()
        }

        protected override fun doInBackground(vararg arg: String?): List<String> {
            var out_result: List<String> = listOf()
            var result: String? = ""

            try {
                /*val nsp = NetworkSecurityPolicy.getInstance()
                val ctp = nsp.isCleartextTrafficPermitted
                Log.d("STATE", "CleartextTrafficPermitted $ctp")*/

                val my_is: InputStream? = getInputStream(
                    "${arg[0]}",
                    "user",
                    "password"
                )

                val my_br = BufferedReader(InputStreamReader(my_is))

                my_br.use {
                    it.lines().forEach { line ->
                        result += line
                    }
                }
            }
            catch ( ex: Exception ) {
                Log.e("ERROR", ex.message.toString())
                //ex.printStackTrace()
            }

            try {
                //Log.d("OUTPUT", "Result: $result")

                if( arg[1] == "1" ) out_result = parseResponse1( result )
                else if ( arg[1] == "2" ) out_result = parseResponse2( result )
            }
            catch ( ex: Exception ) {
                Log.e("ERROR", ex.message.toString())
                //ex.printStackTrace()
            }

            return out_result
        }

        override fun onPostExecute(result: List<String>) {
            //Log.d("OUTPUT", "Result: $s")
            try {
                mListener?.myMethod(result);
            }
            catch ( ex: Exception ) {
                Log.e("ERROR", ex.message.toString())
                //ex.printStackTrace()
            }
        }

        @Throws(IOException::class)
        private fun getInputStream(urlStr: String, user: String, password: String): InputStream? {
            val url = URL(urlStr)
            val conn: HttpsURLConnection = url.openConnection() as HttpsURLConnection

            // Create the SSL connection
            val sc: SSLContext = SSLContext.getInstance("TLS")
            sc.init(null, null, SecureRandom())
            conn.setSSLSocketFactory(sc.getSocketFactory())

            // Use this if you need SSL authentication
            /*val userpass = "$user:$password"
            val basicAuth = "Basic " + Base64.encodeToString(userpass.toByteArray(), Base64.DEFAULT)
            conn.setRequestProperty("Authorization", basicAuth)*/

            // set Timeout and method
            conn.readTimeout = 5000
            conn.connectTimeout = 5000
            conn.requestMethod = "POST"
            conn.doInput = true

            // Add any data you wish to post here
            conn.connect()
            return conn.inputStream
        }

        fun parseResponse1( str: String? ): List<String> {
            if( str == null ) return listOf()

            // Convert String to json object
            val json = JSONObject( str )

            // get LL json object
            val json_LL = json.getJSONObject("artist")
            val json_bio = json_LL.getJSONObject("bio")

            /*Log.d("OUTPUT", "parseResponse:  name: ${json_LL.getString("name" )}")
                //Log.d("OUTPUT", "parseResponse: image: ${json_LL.getJSONArray("image").getJSONObject(3).getString("#text")}")
            Log.d("OUTPUT", "parseResponse: biography: ${json_bio.getString( "summary" )}")
                //Log.d("OUTPUT", "parseResponse: biography all: ${json_bio.getString( "content" )}")*/

            return listOf( json_LL.getString("name" ), json_bio.getString( "summary" ) )
        }

        fun parseResponse2( str: String? ): List<String> {
            if( str == null ) return listOf()

            val json = JSONObject( str )
            val json_TT = json.getJSONObject("topalbums")
            val json_LL = json_TT.getJSONArray("album").getJSONObject(0)

            /*Log.d("OUTPUT", "parseResponse:  album: ${json_LL.getString("name" )}")
                //Log.d("OUTPUT", "parseResponse:  name: ${json_TT.getString("artist" )}")
            Log.d("OUTPUT", "parseResponse:  image: ${json_LL.getJSONArray("image").getJSONObject(3).getString("#text")}")*/

            return listOf( json_LL.getJSONArray("image").getJSONObject(3).getString("#text") )
        }
    }

}