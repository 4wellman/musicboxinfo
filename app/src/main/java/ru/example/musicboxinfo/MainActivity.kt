package ru.example.musicboxinfo

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    var searchString: String? = ""

    companion object {
        const val EXTRA_SEARCH_1 = "ru.example.musicboxinfo.EXTRA_SEARCH_1"
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        searchString = intent.getStringExtra( EXTRA_SEARCH_1 )

        //makeMessage( searchString )
    }

    fun startBiographyActivity( v: View ) {
        val intent = Intent ( this, BiographyActivity::class.java )

        intent.putExtra( BiographyActivity.EXTRA_SEARCH_2, searchString )

        startActivity( intent )
   }

    fun startTrackActivity( v: View ) {
        val intent = Intent ( this, BestActivity::class.java )

        intent.putExtra( BestActivity.EXTRA_SEARCH_3, searchString )

        startActivity( intent )
    }

    fun makeMessage( st: String? ) {
        val myToast = Toast.makeText(this, st, Toast.LENGTH_SHORT )
        myToast.show()
    }

}